(function ( $ ) {
	$.fn.formBuilder = function(options){
        let settings;
        let that = this;
		let reviewerOptions = '';
        settings = $.extend({
            // These are the defaults.
            apiVersion: 'v1',
            accessToken: '',
            templateId: '',
			buttonClasses: 'btn btn-primary',
			successCallback: function(){
				alert('Saved');
			},
			colors: [
				'#80D8FF',
				'#689F38',
				'#FFA000',
				'#616161',
				'#FF80AB',
				'#C2185B',
				'#512DA8',
				'#303F9F',
				'#1976D2',
				'#0288D1',
				'#0097A7',
				'#00796B',
				'#388E3C',
				'#AFB42B',
				'#FBC02D',
				'#F57C00',
				'#E64A19',
				'#5D4037',
				'#455A64',
				'#ff8a80',
				'#EA80FC',
				'#B388FF',
				'#8C9EFF',
				'#82B1FF',
				'#84FFFF',
				'#A7FFEB',
				'#B9F6CA',
				'#CCFF90',
				'#F4FF81',
				'#FFE57F',
				'#FFD180',
				'#FF9E80',
				'#7B1FA2',
				'#D32F2F'
			],
			signerAliases: {},
        }, options);

        $(that).append('<div class="smf-wrapper"><div class="smf-field-type-container"></div><div class="smf-form-container"></div><div class="smf-submit-button-container"></div></div>');

		$(document).keydown(function(e) {
			if($(that).find('.smf-form-field-container.selected').length === 0){
				return;
			}
			switch(e.which) {
				case 46: // delete
				removePopover();
				$(that).find('.smf-form-field-container.selected').remove();
				break;

				case 37: // left
				e.preventDefault();
				var new_value = (parseFloat($(that).find('.smf-form-field-container.selected').css('left').replace('px','')) - 1);
				$(that).find('.smf-form-field-container.selected').css('left', new_value + 'px');
				$(that).find('.smf-form-field-container.selected').data('x', new_value + 'px');
				$('.popover').css('left', (parseFloat($('.popover').css('left').replace('px','')) - 1) + 'px');
				break;

				case 38: // up
				e.preventDefault();
				var new_value = (parseFloat($(that).find('.smf-form-field-container.selected').css('top').replace('px','')) - 1);
				$(that).find('.smf-form-field-container.selected').css('top', new_value + 'px');
				$(that).find('.smf-form-field-container.selected').data('y', new_value + 'px');
				$('.popover').css('top', (parseFloat($('.popover').css('top').replace('px','')) - 1) + 'px');
				break;

				case 39: // right
				e.preventDefault();
				var new_value = (parseFloat($('.smf-form-field-container.selected').css('left').replace('px','')) + 1);
				$(that).find('.smf-form-field-container.selected').css('left', new_value + 'px');
				$(that).find('.smf-form-field-container.selected').data('x', new_value + 'px');
				$('.popover').css('left', (parseFloat($('.popover').css('left').replace('px','')) + 1) + 'px');
				break;

				case 40: // down
				e.preventDefault();
				var new_value = (parseFloat($('.smf-form-field-container.selected').css('top').replace('px','')) + 1);
				$(that).find('.smf-form-field-container.selected').css('top', new_value + 'px');
				$(that).find('.smf-form-field-container.selected').data('y', new_value + 'px');
				$('.popover').css('top', (parseFloat($('.popover').css('top').replace('px','')) + 1) + 'px');
				break;

				default: return; // exit this handler for other keys
			}
			e.preventDefault(); // prevent the default action (scroll / move caret)
		});

		$(that).on('click','.smf-form-layout-submit',function(){
			// Yar! Gather ye daters
			var fields = [];
			$(that).find('.smf-form-field-container').each(function(){
				fields.push({
					id : $(this).data('id'),
					x : $(this).data('x'),
					y : $(this).data('y'),
					width : $(this).width(),
					height : $(this).height(),
					page : $(this).data('page'),
					is_required : $(this).data('is-required'),
					form_signer_id : $(this).data('form-signer-id'),
					name: $(this).data('name'),
					type : $(this).data('type')
				});
			});

			doAjax('https://dev.please.signmyform.com/api/'+settings['apiVersion']+'/templates/'+settings['templateId']+'/fields', 'PUT', {
				fields : fields
			}, function(){
				settings['successCallback']();
			}, settings['accessToken']);
		})

		function removePopover(){
			try{
				$(that).find('.smf-form-field-container.selected').popover('dispose');
			}catch(error){
				try{
					$(that).find('.smf-form-field-container.selected').popover('destroy');
				}catch(error){
					throw "Failed to destroy Popover";
				}
			}
		}

		$(that).on('click','.smf-form-field-container',function(e){
			e.stopPropagation();
		})

		$(that).on('click','.popover',function(e){
			e.stopPropagation();
		});

		$(document).on('keyup change','#form-field-form-name',function(){
			$(that).find('.smf-form-field-container.selected').data('name', $(this).val());
		});

		$(document).on('change','#form-field-form-signer-id',function(){
			$(that).find('.smf-form-field-container.selected').data('form-signer-id', $(this).val());
			$(that).find('.smf-form-field-container.selected').css('border-color',$('#form-field-form-signer-id option:selected').data('color'));
			var o = $(that).find('.smf-form-field-container.selected').css('border-color').replace('rgb', 'rgba');
			o = o.replace(')',',.3)');
			$(that).find('.smf-form-field-container.selected').css('background-color', o);
		});

		$(that).on('mousedown','.smf-form-field-container',function(){
			removePopover();
			$(that).find('.smf-form-field-container.selected').css('background-color', 'transparent');
			$(that).find('.smf-form-field-container').removeClass('selected');
			$(this).addClass('selected');
			var o = $(this).css('border-color').replace('rgb', 'rgba');
			o = o.replace(')',',.3)');
			$(this).css('background-color', o);
			$(this).popover();
			$(this).on('shown.bs.popover',function(){
				$('#form-field-form-name').val($(this).data('name'));
				$('#form-field-form-signer-id').val($(this).data('form-signer-id'));
				if($(this).data('is-required') == 1){
					$('#form-field-is-required').prop('checked',true);
				}else{
					$('#form-field-is-required').prop('checked',false);
				}
			})
		});

		$(that).on('click', '.smf-form-container', function(e){
			removePopover();
			$(that).find('.smf-form-field-container.selected').css('background-color', 'transparent');
			$(that).find('.smf-form-field-container').removeClass('selected');
		});

		$(document).on('change','.smf-form-field-is-required',function(){
			if($(this).is(':checked')){
				$(that).find('.smf-form-field-container.selected').data('is-required',1);
			}else{
				$(that).find('.smf-form-field-container.selected').data('is-required',0);
			}
		});

		$(that).on('click','.smf-pdf-page',function(e){
			if($(that).find('.smf-add-field-selector.selected').length > 0){
				var parentOffset = $(this).offset();
				var x = e.pageX - parentOffset.left;
				var y = e.pageY - parentOffset.top;
				var page = $(this).data('page');
				var type = $(that).find('.smf-add-field-selector.selected').data('type');
				var height;
				var width;

				// Get default sizes for each type
				if(type == 'checkbox' || type == 'radio'){
					height = 15;
					width = 15;
				}else if(type == 'text'){
					height = 21;
					width = 150;
				}else if(type == 'signature'){
					height = 30;
					width = 180;
				}else if(type == 'initial'){
					height = 30;
					width = 90;
				}else if(type == 'sign_date'){
					height = 21;
					width = 180;
				}

				// Center the element around the click
				x = x - width / 2;
				y = y - height / 2;

				// Make sure nothing is out of bounds
				if(x + width > $(this).outerWidth()){
					// Too far right
					x = $(this).outerWidth() - width;
				}
				if(y + height > $(this).outerHeight()){
					// Too far down
					y = $(this).outerHeight() - height;
				}
				if(x - width / 2 < 0){
					// Too far up
					x = 0;
				}
				if(y - height / 2 < 0){
					// Too far left
					y = 0;
				}

				removePopover();
				//$('.form-field-container.selected').removeClass('selected');
				$(this).append('<div data-page="'+page+'" data-toggle="popover" data-content="<form><i class=\'fa fa-trash smf-remove-field\'></i><div class=\'form-group\'><label for=\'form-field-form-signer-id\'>Signer</label><select class=\'form-control\' id=\'form-field-form-signer-id\'>'+reviewerOptions+'</select></div><div class=\'form-group\'><label for=\'form-field-form-name\'>Name</label><input type=\'text\' class=\'form-control\' id=\'form-field-form-name\' /></div><div class=\'form-group\'><div class=\'form-check\'><input type=\'checkbox\' class=\'form-check-input\' id=\'form-field-is-required\'><label class=\'form-check-label\' for=\'form-field-is-required\'>Required</label></div></form>" data-id="" data-form-signer-id="" data-is-required="0" data-placement="right" data-html="true" data-type="'+type+'" data-x="'+(x)+'" data-y="'+(y)+'" class="smf-form-field-container selected" style="border-color:#000000;left:'+x+'px;top:'+y+'px;width:'+width+'px;height:'+height+'px;"><span class="smf-move-handle"></span><p>'+type+'</p></div>');

				$(that).find('.smf-add-field-selector.selected').removeClass('selected');

				$(that).find('.smf-form-field-container').prop('disabled', false).draggable({
					cancel: false,
					handle: '.smf-move-handle'
				});
			}
		});

		$(that).on('click','.smf-add-field-selector',function(){
			$(that).find('.smf-add-field-selector').removeClass('selected');
			$(this).addClass('selected');
		});

		$(document).on('click','.smf-remove-field',function(){
			removePopover();
			$(that).find('.smf-form-field-container.selected').remove();
		});

		/*$(document).on('mouseup', '.smf-form-field-container', function(){
			var that = this;
			setTimeout(function(){
				var smfheight = Math.round(parseFloat($(that).height()));
				var smfwidth = Math.round(parseFloat($(that).width()));
				$(that).outerHeight(smfheight+'px');
				$(that).outerWidth(smfwidth+'px');
				console.log(Math.round(parseFloat($(that).outerHeight())));
				console.log(Math.round(parseFloat($(that).outerWidth())));
				console.log($(that).outerHeight());
				console.log($(that).outerWidth());
			}, 200);
		})*/

        return global = {
            setAccessToken: function(accessToken){
                settings['accessToken'] = accessToken;
            },
            load: function(templateId, options){
				if(typeof options['signerAliases'] !== 'undefined'){
					settings['signerAliases'] = options['signerAliases'];
				}
				reviewerOptions = '';
                settings['templateId'] = templateId;
				$(that).find('.smf-submit-button-container').empty();
				$(that).find('.smf-form-container').empty();
				$(that).find('.smf-field-type-container').empty();

                doAjax('https://please.signmyform.com/api/'+settings['apiVersion']+'/templates/'+templateId+'/pages', 'GET', {}, function(response){
                    for(var x=1; x<=response.data.page_count; x++){
                        $(that).find('.smf-form-container').append('<div class="smf-pdf-page smf-pdf-page-'+x+' ui-droppable" data-page="'+x+'"><img src="'+response.data.signed_urls[x]+'" /></div>');
                    }
					$(that).find('.smf-submit-button-container').append('<button type="button" class="'+settings['buttonClasses']+' smf-form-layout-submit">Save</button>');
					$(that).find('.smf-field-type-container').append(`
						<span class="smf-add-field-selector" id="add-signature" data-type="signature"><i class="fal fa-signature" style="margin-right:5px;"></i> Signature</span>
						<span class="smf-add-field-selector" id="add-date" data-type="sign_date"><i class="fal fa-calendar-day" style="margin-right:5px;"></i> Sign Date</span>
						<span class="smf-add-field-selector" id="add-initial" data-type="initial"><i class="fal fa-font-case" style="margin-right:5px;"></i> Initial</span>
						<span class="smf-add-field-selector" id="add-text" data-type="text"><i class="fal fa-text-size" style="margin-right:5px;"></i> Text</span>
						<span class="smf-add-field-selector" id="add-checkbox" data-type="checkbox"><i class="fal fa-check-square" style="margin-right:5px;"></i> Checkbox</span>
						<span class="smf-add-field-selector" id="add-radio" data-type="radio"><i class="fal fa-check-circle" style="margin-right:5px;"></i> Radio</span>
						`);
					doAjax('https://please.signmyform.com/api/'+settings['apiVersion']+'/templates/'+templateId+'/signers', 'GET', {}, function(response){
						reviewerOptions += '<option value=\'\' data-color=\'#000000\'></option>';
						$.each(response.data, function(k, v){
							var name;
							if(v.name == -1){
								if(typeof settings['signerAliases'][v.sort_order] === 'undefined'){
									reviewerOptions += '<option value=\''+v.sort_order+'\' data-color=\''+settings['colors'][v.sort_order]+'\'>Dynamic</option>';
								}else{
									reviewerOptions += '<option value=\''+v.sort_order+'\' data-color=\''+settings['colors'][v.sort_order]+'\'>'+settings['signerAliases'][v.sort_order]+'</option>';
								}
							}else{
								reviewerOptions += '<option value=\''+v.sort_order+'\' data-color=\''+settings['colors'][v.sort_order]+'\'>'+v.first_name+' '+v.last_name+'</option>';
							}
						});
	                    doAjax('https://please.signmyform.com/api/'+settings['apiVersion']+'/templates/'+templateId+'/fields', 'GET', {}, function(response){
	                        $.each(response.data, function(k, v){
	                            $(that).find('.smf-pdf-page-'+v.page).append('<div data-toggle="popover" data-content="<form><i class=\'fa fa-trash smf-remove-field\'></i><div class=\'form-group\'><label for=\'form-field-form-signer-id\'>Signer</label><select class=\'form-control\' id=\'form-field-form-signer-id\'>'+reviewerOptions+'</select></div><div class=\'form-group\'><label for=\'form-field-form-name\'>Name</label><input type=\'text\' class=\'form-control\' id=\'form-field-form-name\' /></div><div class=\'form-group\'><div class=\'form-check\'><input type=\'checkbox\' class=\'form-check-input smf-form-field-is-required\'><label class=\'form-check-label\' for=\'form-field-is-required\'>Required</label></div></form>" data-is-required="'+v.is_required+'" data-placement="right" data-html="true" data-page="'+v.page+'" data-type="'+v.type+'" data-x="'+Math.round(parseFloat(v['x']))+'" data-name="'+v.name+'" data-y="'+Math.round(parseFloat(v['y']))+'" data-id="'+v.id+'" data-form-signer-id="'+v.signer+'" id="form-template-'+v.id+'" name="form-template-'+v.id+'" class="smf-form-field-container" data-form-field-definition-id="'+v.id+'" style="border-color:'+settings['colors'][v.signer]+';left:'+Math.round(parseFloat(v.x))+'px;top:'+Math.round(parseFloat(v.y))+'px;width:'+Math.round(parseFloat(v.width))+'px;height:'+Math.round(parseFloat(v.height))+'px;" disabled="" data-original-title="" title=""><span class="smf-move-handle"></span><p>'+v.type+'</p></div>');
	                        });

							$(that).find('.smf-form-field-container').prop('disabled', false).draggable({
								cancel: false,
								handle: '.smf-move-handle'
							});

							$(that).find('.smf-pdf-page').droppable({
								drop : function(event, ui){
									var x = ui.offset.left - $(this).offset().left - 1;
									var y = ui.offset.top - $(this).offset().top - 1;
									var page = $(this).data('page');
									var width = ui.draggable.width;
									var height = ui.draggable.height;
									ui.draggable
										.detach()
										.appendTo('.smf-pdf-page-'+$(this).data('page'))
										.css({
											top : y,
											left : x
										})
										.data('x',(x))
										.data('y',(y))
										.data('width',width)
										.data('height',height)
										.data('page',page);
									if(x < 0 || y < 0){
										alert('Be sure the field is placed within the form');
									}else{

									}
								}
							});
	                    }, settings['accessToken']);
	                }, settings['accessToken']);
				}, settings['accessToken']);
            }
        };
    }

    function doAjax(url, type, data, always, accessToken, dataType){
        var image = true;
        var ajax = {
            url: url,
            type: type,
            data: data,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + accessToken);
            },
            success: function(response){
                always(response);
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {

            }
        }
        if(typeof dataType === 'undefined'){
            dataType = 'json';
        }
        ajax['dataType'] = dataType;
        $.ajax(ajax);
    }
}(jQuery));
